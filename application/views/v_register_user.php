<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?=$title;?></title>

	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Parsley CSS -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/parsleyjs/src/parsley.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/dist/css/AdminLTE.min.css">
	<!-- Sweetalert2 -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/sweetalert2/dist/sweetalert2.min.css">
	<!-- Animate css -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/animate.css/animate.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/plugins/iCheck/square/blue.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<style>
		ul {
			list-style-type: disc;
			margin-left: 18px;
			padding: 0;
		}

		li {
			float: left;
			font-size: 15px;
		}

		input.parsley-error,
		select.parsley-error,
		textarea.parsley-error {    
			border-color:#843534;
			box-shadow: none;
		}

		input.parsley-error:focus,
		select.parsley-error:focus,
		textarea.parsley-error:focus {    
			border-color:#843534;
			box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 6px #ce8483
		}
	</style>
	
</head>
<body class="hold-transition register-page">
<div class="wrapper">
	<div class="register-logo">
		<a class="" style="text-decoration:none">FORM REGISTRASI USER</a>
	</div>

	<div class="">
		<div class="container-fluid">
			<div class="row">

				<!-- Form Registrasi Badan Usaha -->
				<div class="col-sm-4 col-xs-offset-4">
					<div class="well">
						<?php echo form_open('save', array('id'=>'register', 'data-parsley-validate'=>'')); ?>

							<!-- Data User -->
							<div class="form-group has-feedback">
								<label for="fullname">Nama Pendaftar</label>
								<input type="text" class="form-control" id="fullname" name="fullname" placeholder="Isikan Nama Lengkap Anda" data-parsley-required="true" value="<?=set_value('fullname');?>" data-parsley-ui-enabled="true">
								<span class="fa fa-user form-control-feedback"></span>
							</div>

							<!-- Nomor Handphone User -->
							<div class="form-group has-feedback">
								<label for="phone">No Handphone</label>
								<input type="text" class="form-control" id="phone" name="phone" placeholder="Isikan Nomor Handphone Anda" value="<?=set_value('phone');?>" data-parsley-required="true" data-parsley-trigger="keyup" data-parsley-ui-enabled="true">
								<span class="fa fa-phone form-control-feedback"></span>
								<?php echo form_error('phone', '<small class="text-danger">', '</small>'); ?>
							</div>

							<!-- Email User -->
							<div class="form-group has-feedback">
								<label for="email">Email</label>
								<input type="email" class="form-control" id="email" name="email" placeholder="Isikan Email Anda Yang Valid" data-parsley-required="true" value="<?=set_value('email');?>" data-parsley-trigger="keyup" data-parsley-ui-enabled="true">
								<span class="fa fa-envelope-o form-control-feedback"></span>
								<?php echo form_error('email', '<small class="text-danger">', '</small>'); ?>
							</div>

							<!-- Password User -->
							<div class="form-group has-feedback">
								<label for="pwd">Password</label>
								<input type="password" class="form-control" id="password" name="password" placeholder="Isikan Password minimal 8 karakter" data-parsley-required="true" data-parsley-min="8" value="<?=set_value('password');?>" data-parsley-trigger="keyup" data-parsley-ui-enabled="true">
								<span class="fa fa-lock form-control-feedback"></span>
								<?php echo form_error('password', '<small class="text-danger">', '</small>'); ?>
							</div>

							<!-- Konfirmasi Password User -->
							<div class="form-group has-feedback">
								<label for="conf_pwd">Konfirmasi Password</label>
								<input type="password" class="form-control" id="conf_pwd" name="conf_pwd" placeholder="Isikan Konfirmasi Password" data-parsley-required="true" data-parsley-equalto="#password" value="<?=set_value('conf_pwd');?>" data-parsley-trigger="keyup" data-parsley-ui-enabled="true">
								<span class="fa fa-lock form-control-feedback"></span>
								<?php echo form_error('conf_pwd', '<small class="text-danger">', '</small>'); ?>
							</div>

							<div class="row">
								<div class="col-xs-6">
									<div class="checkbox icheck">
										<p>Already signed up? <strong><a href="<?=site_url('masuk');?>">Sign In</a></strong></p>
									</div>
								</div>
								<!-- /.col -->
								<div class="col-xs-6 text-right">
									<button type="submit" class="btn btn-primary btn-flat" name="btn-submit" id="btn-submit"><i class="fa fa-user-plus"></i> Registrasi</button>
								</div>
								<!-- /.col -->
							</div>
							<!-- /.col -->
						<?php echo form_close(); ?>
					</div>
				</div>
				<!-- /.col-sm-6 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /.register-box -->
</div>
<!-- /.wrapper -->

<!-- jQuery 3 -->
<script src="<?=base_url();?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url();?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=base_url();?>vendor/igorescobar/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
<!-- iCheck -->
<script src="<?=base_url();?>assets/adminlte/plugins/iCheck/icheck.min.js"></script>
<!-- Parsley Validation -->
<script src="<?=base_url();?>assets/adminlte/bower_components/parsleyjs/dist/parsley.min.js"></script>
<script src="<?=base_url();?>assets/adminlte/bower_components/parsleyjs/dist/i18n/id.js"></script>
<!-- Sweetalert2 -->
<script src="<?=base_url();?>assets/adminlte/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
<script type="text/javascript">
$(function() {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' /* optional */
	});

	// Mask Input
	$("#npwp").mask("99.999.999.9-999.999", { placeholder: 'Isikan NPWP Perusahaan Anda' });
	$("#phone").mask("0000000000000", { placeholder: 'contoh: 081234567890' });

	$('#propinsi').change(function() {
		var id_propinsi = $(this).val();

		$.ajax({
			type: "POST",
			url: "<?=base_url('index.php/auth/getKab/');?>" + id_propinsi,
			cache: false,
			success: function(respond)
			{
				$('#kabupaten').html(respond);
			}
		});
	});

	$('#register').parsley();

	// sweetalert2 + animate.css
	<?php if ($this->session->flashdata('error')): ?>
		swal({
			title: 'Error',
			html: $('<div>')
				.addClass('alert alert-danger')
				.text("<?=$this->session->flashdata('error');?>"),
			animation: false,
			customClass: 'animated tada'
		});
	<?php elseif ($this->session->flashdata('success')): ?>
		swal({
			position: 'top-end',
			type: 'success',
			title: "<?=$this->session->flashdata('success');?>",
			showConfirmButton: false,
			timer: 3000
		});
	<?php endif; ?>

});
</script>

</body>
</html>
