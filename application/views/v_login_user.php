<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?=$title;?></title>

	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Parsley CSS -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/parsleyjs/src/parsley.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/dist/css/AdminLTE.min.css">
	<!-- Sweetalert2 -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/sweetalert2/dist/sweetalert2.min.css">
	<!-- Animate css -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/animate.css/animate.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/plugins/iCheck/square/blue.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body class="hold-transition login-page">
<div class="login-box">
	<div class="login-logo">
		<a href="<?=base_url();?>">GAPENSI</a>
	</div>

	<!-- /.login-logo -->
	<div class="login-box-body">
		<p class="login-box-msg">FORM LOGIN <?=$msg;?></p>

		<!-- Open Form -->
		<?php echo form_open('validate', array('id'=>'formLogin', 'data-parsley-validate'=>'')); ?>
			<!-- Email / NPWP -->
			<div class="form-group has-feedback">
				<label for="email">Email</label>
				<input type="email" name="email" id="email" class="form-control" value="<?=set_value('email');?>" placeholder="Masukkan email yang valid" autocomplete="off" data-parsley-required="true" data-parsley-trigger="keyup" data-parsley-ui-enabled="true">
				<span class="fa fa-envelope-o form-control-feedback"></span>
			</div>

			<!-- Password -->
			<div class="form-group has-feedback">
				<label for="password">Password</label>
				<input type="password" class="form-control" name="password" id="password" placeholder="Password account anda" autocomplete="off" data-parsley-required="true" data-parsley-trigger="keyup" data-parsley-minlength="8" data-parsley-ui-enabled="true">
				<span class="fa fa-lock form-control-feedback"></span>
			</div>

			<!-- Captcha Code -->
			<div class="form-group has-feedback">
				<label for="captcha">Captcha</label>
				<input type="text" name="captcha" id="captcha" class="form-control"placeholder="Masukkan angka captcha dibawah" required>
				<!-- <br> -->
				<?=$captcha['image'];?>
			</div>
			<div class="row">
				<div class="col-xs-8">
					<div class="checkbox icheck">
						<label>
							<input type="checkbox" name="remember"> Remember Me
						</label>
					</div>
				</div>
				<!-- /.col-xs-8 -->

				<div class="col-xs-4">
					<button type="submit" name="btn-submit" id="btn-submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Sign In</button>
				</div>
				<!-- /.col-xs-4 -->
			</div>

		<?php echo form_close(); ?>
		<!-- Closed Form -->

		<!-- <a href="#">I forgot my password</a><br> -->
		<a href="<?=site_url('daftar');?>" class="text-center">Register a new membership</a>

	</div> <!-- /.login-box-body -->
</div> <!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?=base_url();?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url();?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?=base_url();?>assets/adminlte/plugins/iCheck/icheck.min.js"></script>
<!-- Parsley Validation -->
<script src="<?=base_url();?>assets/adminlte/bower_components/parsleyjs/dist/parsley.min.js"></script>
<script src="<?=base_url();?>assets/adminlte/bower_components/parsleyjs/dist/i18n/id.js"></script>
<!-- Sweetalert2 -->
<script src="<?=base_url();?>assets/adminlte/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>

<script type="text/javascript">
$(function () {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' /* optional */
	});

	// sweetalert2 + animate.css
	<?php if ($this->session->flashdata('error')): ?>
		swal({
			title: 'Error',
			html: $('<div>')
				.addClass('alert alert-danger')
				.text("<?=$this->session->flashdata('error');?>"),
			animation: false,
			customClass: 'animated tada'
		});
	<?php elseif ($this->session->flashdata('success')): ?>
		swal({
			position: 'top-end',
			type: 'success',
			title: "<?=$this->session->flashdata('success');?>",
			showConfirmButton: false,
			timer: 3000
		});
	<?php endif; ?>

	$('#formLogin').parsley();

});


</script>
</body>
</html>
