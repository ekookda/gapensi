<!-- Content Header (Page header) -->
<section class="content-header">
<h1>
	Dashboard
	<small>Control panel</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li class="active">Dashboard</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Pendaftaran Badan Usaha</h3>
            </div>

            <div class="box-body">
                <div class="table-responsive">
                    <table id="table-perusahaan" class="table table-hover table-condensed table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Nama Badan Usaha</th>
                            <th class="text-center">Bentuk Usaha</th>
                            <th class="text-center">Telp / FAX</th>
                            <th class="text-center">Alamat Email</th>
                            <th class="text-center">Nomor NPWP</th>
                            <th class="text-center">Status Proses</th>
                            <th class="text-center">Verifikasi Email</th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach ($data_usaha as $r):
                        switch ($r->status_process)
                        {
                            case 'pending':
                                $status = '<span class="label label-warning">'.$r->status_process.'</span>';
                                break;
                            case 'success':
                                $status = '<span class="label label-success">'.$r->status_process.'</span>';
                                break;
                            default:
                                $status = '<span class="label label-danger">'.$r->status_process.'</span>';
                            break;
                        }

                        if ($r->verification_email == 1) {
                            $verify = 'Y';
                        }
                        else
                        {
                            $verify = 'N';
                        }
                    ?>
                        <tr class="text-center">
                            <td><?=$no++;?></td>
                            <td><?=$r->company_name;?></td>
                            <td><?=$r->nm_groups;?></td>
                            <td><?=$r->phone;?></td>
                            <td><?=$r->email;?></td>
                            <td><?=$r->no_npwp;?></td>
                            <td><?=$status;?></td>
                            <td><?=$verify;?></td>
                            <td><?=anchor('user/anggota-profile/' . $r->id_company, '<i class="fa fa-eye"></i>');?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col-xs-12 -->

</div>
<!-- /.row (main row) -->

</section>
<!-- /.content -->
