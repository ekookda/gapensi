<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<!-- Small boxes (Stat box) -->
	<div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Pendaftaran Badan Usaha</h3>
                </div>
				<!-- /.box-header -->
				<!-- form start -->
				<?php echo form_open('registration-validation', array('class'=>'form-horizontal', 'id'=>'formRegist', 'data-parsley-validate'=>'')); ?>
					<div class="box-body">
						<div class="row">
							<div class="col-sm-6">
								<!-- Bentuk Usaha -->
								<div class="form-group has-feedback">
									<label for="kategori" class="col-sm-4 control-label">Bentuk Badan Usaha</label>
									<div class="col-sm-8">
										<select id="kategori" name="kategori" class="form-control" data-parsley-required="true">
											<option value="">-- Pilih Bentuk Usaha --</option>
											<?php
												$query = $this->db->get('tb_groups_business')->result();
												foreach ($query as $row):
											?>
											<option value="<?=$row->id_groups;?>"><?=$row->nm_groups;?></option>
											<?php endforeach; ?>
										</select>
										<?php echo form_error('usaha', '<small class="text-danger">', '</small>'); ?>
									</div>
								</div>

								<!-- Nama Badan Usaha -->
								<div class="form-group has-feedback">
									<label for="company_name" class="col-sm-4 control-label">Nama Badan Usaha</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="company_name" name="company_name" placeholder="Nama Badan Usaha (Tanpa Bentuk Badan Usaha)" value="<?=set_value('company_name');?>" data-parsley-required="true" data-parsley-trigger="keyup" data-parsley-ui-enabled="true">
										<span class="fa fa-building-o form-control-feedback"></span>
										<?php echo form_error('company_name', '<small class="text-danger">', '</small>'); ?>
									</div>
								</div>

								<!-- Nomor NPWP -->
								<div class="form-group has-feedback">
									<label for="npwp" class="col-sm-4 control-label">Nomor NPWP</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="npwp" name="nomor_npwp" placeholder="Isikan NPWP Perusahaan Anda" value="<?=set_value('nomor_wpwp');?>" data-parsley-required="true" data-parsley-trigger="keyup" data-parsley-ui-enabled="true">
										<span class="fa fa-drivers-license-o form-control-feedback"></span>
										<?php echo form_error('nomor_npwp', '<small class="text-danger">', '</small>'); ?>
									</div>
								</div>

								<!-- Pilih Propinsi -->
								<div class="form-group has-feedback">
									<label for="propinsi" class="col-sm-4 control-label">Provinsi</label>
									<div class="col-sm-8">
										<select id="propinsi" name="propinsi" class="form-control" data-parsley-required="true">
											<option value="">-- Pilih Propinsi --</option>
											<?php
											$propinsi = $this->db->get('tb_propinsi')->result();
											foreach($propinsi as $p)
											{
												echo "<option value='".$p->id_propinsi."'>$p->nama_propinsi</option>";
											}
											?>
										</select>
										<?php echo form_error('propinsi', '<small class="text-danger">', '</small>'); ?>
									</div>
								</div>

								<!-- Kabupaten -->
								<div class="form-group has-feedback">
									<label for="kabupaten" class="col-sm-4 control-label">Kabupaten</label>
									<div class="col-sm-8">
										<select id="kabupaten" name="kabupaten" class="form-control" data-parsley-required="true">
											<option value="">-- Pilih Kabupaten --</option>
										</select>
										<?php echo form_error('kabupaten', '<small class="text-danger">', '</small>'); ?>
									</div>
								</div>

								<!-- Nomor Handphone -->
								<div class="form-group has-feedback">
									<label for="phone" class="col-sm-4 control-label">No Handphone</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="phone" name="phone" placeholder="Nomor Handphone" data-parsley-required="true" value="<?=set_value('phone');?>" required data-parsley-trigger="keyup">
										<span class="fa fa-phone form-control-feedback"></span>
										<?php echo form_error('phone', '<small class="text-danger">', '</small>'); ?>
									</div>
								</div>

								<!-- Alamat Email -->
								<div class="form-group has-feedback">
									<label for="email" class="col-sm-4 control-label">Alamat Email</label>
										<div class="col-sm-8">
										<input type="email" class="form-control" id="email" name="email" placeholder="Alamat Email Yang Valid" value="<?=set_value('email');?>" data-parsley-required="true" data-parsley-type="email" data-parsley-trigger="keyup" data-parsley-ui-enabled="true"><span class="fa fa-envelope-square form-control-feedback"></span>
										<?php echo form_error('email', '<small class="text-danger">', '</small>'); ?>
									</div>
								</div>

								<!-- Password -->
								<div class="form-group has-feedback">
									<label for="password" class="col-sm-4 control-label">Password</label>
									<div class="col-sm-8">
										<input type="password" class="form-control" name="password" id="password" placeholder="Password" data-parsley-minlength="8" data-parsley-required="true" data-parsley-trigger="keyup" data-parsley-ui-enabled="true">
										<span class="fa fa-key form-control-feedback"></span>
										<?php echo form_error('password', '<small class="text-danger">', '</small>'); ?>
									</div>
								</div>

								<!-- Konfirmasi Password -->
								<div class="form-group has-feedback">
									<label for="confirm_pwd" class="col-sm-4 control-label">Ulangi Password</label>
									<div class="col-sm-8">
										<input type="password" name="confirm_pwd" id="confirm_pwd" class="form-control" placeholder="Ulangi Password Anda" data-parsley-trigger="keyup" data-parsley-required="true" data-parsley-equalto="#password">
										<span class="fa fa-lock form-control-feedback"></span>
										<?php echo form_error('confirm_pwd', '<small class="text-danger">', '</small>'); ?>
									</div>
								</div>
								<!-- /.col -->
								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4 text-right">
										<button type="submit" class="btn btn-primary btn-flat" name="btn-submit" id="btn-submit"><i class="fa fa-user-plus"></i> Proses Registrasi</button>
									</div>
								</div>
								<!-- /.col -->
							</div>

							<div class="col-sm-6">
								<p class="">Berikut tata cara registrasi online untuk badan usaha :</p>
								<ul class="text-justify">
									<li>Pilih bentuk badan usaha</li>
									<li>Isikan nama badan usaha sesuai dengan akte badan usaha tanpa mengisi bentuk badan usaha</li>
									<li>Isikan Nomor NPWP Perusahaan Anda, Nomor NPWP diperlukan untuk mengecek apakah perusahaan anda sudah pernah melakukan permohonan KTA di GAPENSI, apabila perusahaan anda sudah pernah melakukan permohonan KTA, selanjutnya anda akan diminta untuk melakukan verifikasi perusahaan anda dengan memasukkan alamat email anda. Kami akan mengirimkan password sebagai akses untuk login ke sistem GAPENSI online.</li>
									<li>Pilih nama Provinsi sesuai dengan domisili badan usaha</li>
									<li>Pilih nama Kabupaten</li>
									<li>Email yang anda masukkan akan digunakan sebagai username login dan media utama pengiriman informasi status permohonan badan usaha anda, pastikan email yang digunakan adalah valid</li>
									<li>Nomor handphone yang anda masukkan akan digunakan sebagai media tambahan untuk menyampaikan informasi permohonan badan usaha anda, pastikan anda memasukan nomor yang benar</li>
									<li>Setelah membuat account online, anda akan menerima tautan untuk verifikasi alamat email anda, mohon periksa email anda dan lakukan verifikasi dengan meng - klik tautan yang kami kirim, selanjutnya account anda akan aktif dan anda dapat melakukan proses login di halaman login</li>
								</ul>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->

					</div>
					<!-- /.box-body -->
				<?php echo form_close(); ?>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-xs-12 -->
	</div>
	<!-- /.row (main row) -->

</section>
<!-- /.content -->

<script type="text/javascript">
	jQuery(document).ready(function() {
		$('#propinsi').change(function () {
			var id_propinsi = $(this).val();

			$.ajax({
				type: "POST",
				url: "<?php echo base_url('index.php/auth/getKab/'); ?>" + id_propinsi,
				cache: false,
				success: function (respond) {
					$('#kabupaten').html(respond);
				}
			});
		});

		<?php if ($this->session->flashdata('success')): ?>
			swal('Registrasi Berhasil', '<?=$this->session->flashdata('success');?>', 'success');
		<?php elseif ($this->session->flashdata('error')): ?>
			swal('Registrasi Gagal', '<?=$this->session->flashdata('error');?>', 'error');
		<?php endif; ?>
	});
</script>
