<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?=$title;?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Parsley CSS -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/bower_components/parsleyjs/src/parsley.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/dist/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?=base_url();?>assets/adminlte/plugins/iCheck/square/blue.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<style>
		ul {
			list-style-type: disc;
			margin-left: 18px;
			padding: 0;
		}

		li {
			float: left;
			font-size: 15px;
		}

		input.parsley-error,
		select.parsley-error,
		textarea.parsley-error {    
			border-color:#843534;
			box-shadow: none;
		}

		input.parsley-error:focus,
		select.parsley-error:focus,
		textarea.parsley-error:focus {    
			border-color:#843534;
			box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 6px #ce8483
		}
	</style>
	
</head>
<body class="hold-transition register-page">
<div class="wrapper">
	<!-- <div class="register-logo">
		<a href="<?=base_url();?>assets/adminlte/index2.html"><b>Admin</b>LTE</a>
	</div> -->

	<div class="register-box-body">
		<div class="container-fluid">
			<div class="row">

				<!-- Form Registrasi Badan Usaha -->
				<div class="col-sm-6">
					<h4 class="login-box-msg" style="text-decoration:none">FORM REGISTRASI BADAN USAHA</h4>
					
					<div class="well">
						<?php echo form_open('auth/store', array('id'=>'register', 'data-parsley-validate'=>'')); ?>
							<!-- Bentuk Usaha -->
							<div class="form-group has-feedback">
								<label for="kategori">Bentuk Badan Usaha</label>
								<select id="kategori" name="kategori" class="form-control" data-parsley-required="true">
									<option value="">-- Pilih Bentuk Usaha --</option>
									<?php
										$query = $this->db->get('tb_groups_business')->result();
										foreach ($query as $row):
									?>
									<option value="<?=$row->id_groups;?>"><?=$row->nm_groups;?></option>
									<?php endforeach; ?>
								</select>
								<?php echo form_error('usaha', '<small class="text-danger">', '</small>'); ?>
							</div>

							<!-- Nama Badan Usaha -->
							<div class="form-group has-feedback">
								<label for="company_name">Nama Badan Usaha</label>
								<input type="text" class="form-control" id="company_name" name="company_name" placeholder="Nama Badan Usaha (Tanpa Bentuk Badan Usaha)" value="<?=set_value('company_name');?>" data-parsley-required="true">
								<span class="fa fa-building-o form-control-feedback"></span>
								<?php echo form_error('company_name', '<small class="text-danger">', '</small>'); ?>
							</div>

							<!-- Nomor NPWP -->
							<div class="form-group has-feedback">
								<label for="npwp">Nomor NPWP</label>
								<input type="text" class="form-control" id="npwp" name="nomor_npwp" placeholder="Isikan NPWP Perusahaan Anda" value="<?=set_value('nomor_wpwp');?>" data-parsley-required="true">
								<span class="fa fa-drivers-license-o form-control-feedback"></span>
								<?php echo form_error('nomor_npwp', '<small class="text-danger">', '</small>'); ?>
							</div>

							<!-- Pilih Propinsi -->
							<div class="form-group has-feedback">
								<label for="propinsi">Provinsi</label>
								<select id="propinsi" name="propinsi" class="form-control" data-parsley-required="true">
									<option value="">-- Pilih Propinsi --</option>
									<?php
									foreach($propinsi as $p)
									{
										echo "<option value='".$p->id_propinsi."'>$p->nama_propinsi</option>";
									}
									?>
								</select>
								<?php echo form_error('propinsi', '<small class="text-danger">', '</small>'); ?>
							</div>

							<!-- Kabupaten -->
							<div class="form-group has-feedback">
								<label for="kabupaten">Kabupaten</label>
								<select id="kabupaten" name="kabupaten" class="form-control" data-parsley-required="true">
									<option value="">-- Pilih Kabupaten --</option>
								</select>
								<?php echo form_error('kabupaten', '<small class="text-danger">', '</small>'); ?>
							</div>

							<!-- Nomor Handphone -->
							<div class="form-group has-feedback">
								<label for="phone">No Handphone</label>
								<input type="text" class="form-control" id="phone" name="phone" placeholder="Nomor Handphone" data-parsley-required="<?=set_value('phone');?>" required data-parsley-trigger="keyup">
								<span class="fa fa-phone form-control-feedback"></span>
								<?php echo form_error('phone', '<small class="text-danger">', '</small>'); ?>
							</div>

							<!-- Alamat Email -->
							<div class="form-group has-feedback">
								<label for="email">Alamat Email</label>
								<input type="email" class="form-control" id="email" name="email" placeholder="Alamat Email Yang Valid" value="<?=set_value('email');?>" data-parsley-required="true" data-parsley-type="email"><span class="fa fa-envelope-square form-control-feedback"></span>
								<?php echo form_error('email', '<small class="text-danger">', '</small>'); ?>
							</div>

							<!-- Password -->
							<div class="form-group has-feedback">
								<label for="password">Password</label>
								<input type="password" class="form-control" name="password" id="password" placeholder="Password" data-parsley-minlength="8" data-parsley-required="true">
								<span class="fa fa-key form-control-feedback"></span>
								<?php echo form_error('password', '<small class="text-danger">', '</small>'); ?>
							</div>

							<!-- Konfirmasi Password -->
							<div class="form-group has-feedback">
								<label for="confirm_pwd">Ulangi Password</label>
								<input type="password" name="confirm_pwd" id="confirm_pwd" class="form-control" placeholder="Ulangi Password Anda" data-parsley-minlength="8" data-parsley-required="true" data-parsley-equalto="#password">
								<span class="fa fa-lock form-control-feedback"></span>
								<?php echo form_error('confirm_pwd', '<small class="text-danger">', '</small>'); ?>
							</div>
							<!-- /.col -->

							<div class="row">
								<div class="col-xs-5">
									<div class="checkbox icheck">
										<p>Already signed up? <strong><a href="<?=site_url('auth/login');?>">Sign In</a></strong></p>
									</div>
								</div>
								<!-- /.col -->
								<div class="col-xs-7 text-right">
									<button type="submit" class="btn btn-primary btn-flat" name="btn-submit" id="btn-submit"><i class="fa fa-user-plus"></i> Proses Registrasi</button>
								</div>
								<!-- /.col -->
							</div>
						<?php echo form_close(); ?>
					</div>
					<!-- /.well -->
				</div>
				<!-- /.col-sm-6 -->

				<!-- Pengumuman Cara Registrasi Badan Usaha -->
				<div class="col-sm-6">
					<h4 class="login-box-msg text-left" style="text-decoration:none">Cara Registrasi Badan Usaha</h4>
					<hr>

					<p class="">Berikut tata cara registrasi online untuk badan usaha :</p>

					<ul class="text-justify">
						<li>Pilih bentuk badan usaha</li>
						<li>Isikan nama badan usaha sesuai dengan akte badan usaha tanpa mengisi bentuk badan usaha</li>
						<li>Isikan Nomor NPWP Perusahaan Anda, Nomor NPWP diperlukan untuk mengecek apakah perusahaan anda sudah pernah melakukan permohonan KTA di GAPENSI, apabila perusahaan anda sudah pernah melakukan permohonan KTA, selanjutnya anda akan diminta untuk melakukan verifikasi perusahaan anda dengan memasukkan alamat email anda. Kami akan mengirimkan password sebagai akses untuk login ke sistem GAPENSI online.</li>
						<li>Pilih nama Provinsi sesuai dengan domisili badan usaha</li>
						<li>Pilih nama Kabupaten</li>
						<li>Email yang anda masukkan akan digunakan sebagai username login dan media utama pengiriman informasi status permohonan badan usaha anda, pastikan email yang digunakan adalah valid</li>
						<li>Nomor handphone yang anda masukkan akan digunakan sebagai media tambahan untuk menyampaikan informasi permohonan badan usaha anda, pastikan anda memasukan nomor yang benar</li>
						<li>Setelah membuat account online, anda akan menerima tautan untuk verifikasi alamat email anda, mohon periksa email anda dan lakukan verifikasi dengan meng - klik tautan yang kami kirim, selanjutnya account anda akan aktif dan anda dapat melakukan proses login di halaman login</li>
					</ul>
				</div>
				<!-- /.col-sm-6 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /.register-box -->
</div>
<!-- /.wrapper -->

<!-- jQuery 3 -->
<script src="<?=base_url();?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url();?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=base_url();?>vendor/igorescobar/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
<!-- iCheck -->
<script src="<?=base_url();?>assets/adminlte/plugins/iCheck/icheck.min.js"></script>
<!-- Parsley Validation -->
<script src="<?=base_url();?>assets/adminlte/bower_components/parsleyjs/dist/parsley.min.js"></script>
<script src="<?=base_url();?>assets/adminlte/bower_components/parsleyjs/dist/i18n/id.js"></script>
<script type="text/javascript">
$(function() {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' /* optional */
	});

	// Mask Input
	$("#npwp").mask("99.999.999.9-999.999", { placeholder: 'Isikan NPWP Perusahaan Anda' });
	$("#phone").mask("0000000000000", { placeholder: 'contoh: 081234567890' });

	$('#propinsi').change(function() {
		var id_propinsi = $(this).val();

		$.ajax({
			type: "POST",
			url: "<?=base_url('index.php/auth/getKab/');?>" + id_propinsi,
			cache: false,
			success: function(respond)
			{
				$('#kabupaten').html(respond);
			}
		});
	});

	$('#formLogin').parsley();
});

</script>

</body>
</html>