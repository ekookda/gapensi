<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=base_url();?>assets/adminlte/dist/img/user_128x128.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>
                    <?php
                        $nama_user = $this->session->userdata('name');
                        echo ucwords($this->session->userdata('name'));
                    ?>
                </p>

                <a><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                            <i class="fa fa-search"></i>
                        </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <!-- TreeView Sidebar -->
            <!-- <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                    <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                </ul>
            </li> -->

            <!-- TreeView Single -->
            <?php if ($this->session->userdata('level') == 2): ?>
            <li>
                <a href="<?=site_url('user');?>">
                    <i class="fa fa-building-o"></i> <span>Data Badan Usaha</span>
                    <!-- <span class="pull-right-container">
                        <small class="label pull-right bg-green">new</small>
                    </span> -->
                </a>
            </li>
            <li>
                <a href="<?=site_url('registrasi-perusahaan');?>">
                    <i class="fa fa-vcard-o"></i> <span>Registrasi Perusahaan</span>
                    <!-- <span class="pull-right-container">
                        <small class="label pull-right bg-green">new</small>
                    </span> -->
                </a>
            </li>
            <?php endif; ?>
            <!-- menu registrasi company -->
            <li>
                <a href="<?=site_url('member/upload');?>">
                    <i class="fa fa-upload"></i> <span>Upload File</span>
                    <!-- <span class="pull-right-container">
                        <small class="label pull-right bg-green">new</small>
                    </span> -->
                </a>
            </li>
        </ul>
    </section> <!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
