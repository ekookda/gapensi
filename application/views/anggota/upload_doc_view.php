<!-- Conten Wrapper. Contains page content -->
<div class="">
	<!-- Content Header (Page Header) -->
	<section class="content-header">
		<h1>
			Welcome, <?= ucwords($this->session->userdata('username')); ?>
		</h1>
	
		<ol class="breadcrumb">
			<li><a href="<?=site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=site_url('anggota');?>"><?= ucfirst($this->session->userdata('groups')); ?></a></li>
			<li class="active">Upload File</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
                <?= $this->session->flashdata('notif'); ?>

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Upload File Dokumen</h3>
					</div>
					<!-- /.box-header -->
					
					<!-- form start -->
					<?php
					$attribut_text = ['class'=>'form-control'];
					$attribut_form = ['class'=>'form-horizontal', 'id'=>'myform'];
					echo form_open_multipart('member/do_upload', $attribut_form);
					?>
					<div class="box-body">

                        <div class="form-group">
							<label for="tags" class="control-label">&nbsp;</label>
							<div class="col-sm-4">
								<?= form_upload('txt_upload', '', $attribut_text); ?>
							</div>
                        </div>

                        <!-- button form -->
						<div class="form-group">
							<label for="tags" class="control-label">&nbsp;</label>
							<div class="col-sm-2">
                                <?php
                                $attribut_btn = [
                                    'id' => 'btn-upload',
                                    'name' => 'btn-upload',
                                    'class' => 'btn btn-primary btn-flat',
                                    'content' => '<i class="fa fa-save"></i> Simpan',
                                    'type' => 'submit'
                                ];
                                echo form_button($attribut_btn); 
                                ?>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div  class="box-footer">
						<a onclick="window.history.back(-1)" class="btn btn-danger btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
					</div>
					<!-- /. box-footer -->
				<?= form_close(); ?>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (right) -->
		</div>
		<!-- /.row -->
	</section>
</div>
