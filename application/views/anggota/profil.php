<!DOCTYPE html>
<html lang="en">
    <head>
        <title id="title-web">Data Registrasi</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet">
        <style>
            .error {
                color: red;
            }
            .success {
                color: seagreen;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 offset-md-3">
                    <h1 class="text-center">Data Registrasi Anggota</h1>
                    <hr>

                    <div class="table-responsive">
                        
                        <table class="table table-hover">
                            <tr>
                                <td width="">Nomor Registrasi</td>
                                <td>:</td>
                                <!-- Nomor registrasi diambil dari tanggal registrasi dalam bentuk unix -->
                                <td><?=mysql_to_unix($dataAnggota->created_at);?></td>
                            </tr>
                            <tr>
                                <td width="">Username</td>
                                <td>:</td>
                                <td><?=$dataAnggota->username;?></td>
                            </tr>
                            <tr>
                                <td width="180">Nama Perusahaan</td>
                                <td>:</td>
                                <td><?=strtoupper($dataAnggota->nama_perusahaan);?></td>
                            </tr>
                            <tr>
                                <td>Jenis Usaha</td>
                                <td>:</td>
                                <td><?=strtoupper($dataAnggota->jenis_usaha);?></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?=ucwords($dataAnggota->alamat_perusahaan);?></td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>:</td>
                                <td><?=ucwords($dataAnggota->telepon);?></td>
                            </tr>
                            <tr>
                                <td>Fax</td>
                                <td>:</td>
                                <?php
                                if ($dataAnggota->fax == NULL) {
                                    $noFax = '-';
                                } else {
                                    $noFax = $dataAnggota->fax;
                                }
                                ?>
                                <td><?=$noFax;?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>:</td>
                                <td><?=strtolower($dataAnggota->email);?></td>
                            </tr>
                            <tr>
                                <td>Anggota</td>
                                <td>:</td>
                                <td>Gapensi <?=ucwords($dataAnggota->level_anggota);?></td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>:</td>
                                <?php
                                if ($dataAnggota->status == 0) {
                                    $status = "<span class='badge badge-warning'>Progress</span>";
                                } else {
                                    $status = "<span class='badge badge-success'>active</span>";
                                }
                                ?>
                                <td><?=$status;?></td>
                            </tr>
                            
                        </table>

                    </div>
                    
                    <div class="row">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-4">
                            <a href="#" target="blank" id="printOut" type="button" onclick="return false" class="btn btn-danger">Print</a>
                            <a href="<?=site_url('login/signout');?>" id="signOut" type="button" class="btn btn-default">Signout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#printOut').click(function() {
                    $(this).hide();
                    $('#signOut').hide();
                    window.print();
                    $(this).show(); // menampilkan kembali tombol print
                    $('#signOut').show();
                });
            });
        </script>
    </body>
</html>
