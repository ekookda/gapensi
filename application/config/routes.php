<?php defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'auth/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
| -------------------------------------------------------------------------
| Register Member or Login Member
| -------------------------------------------------------------------------
*/
$route['register'] = 'user/create_member';
$route['login'] = 'auth/login';
$route['logout'] = 'auth/signout';

/*
| -------------------------------------------------------------------------
| URI for anggota or member
| Struktur folder => 'path/to/file';
| -------------------------------------------------------------------------
*/
$route['member'] = 'anggota/anggota';
$route['member/(:any)'] = 'anggota/anggota/$1';
$route['member/(:any)/(:any)'] = 'anggota/anggota/$1/$2';

/*
| -------------------------------------------------------------------------
| URI login for user or admin
| -------------------------------------------------------------------------
*/
$route['masuk'] = 'auth_user/signin';
$route['daftar'] = 'auth_user/signup_user';
$route['validate'] = 'auth_user/process_login';
$route['verification/(:any)'] = 'auth_user/verification/$1';
$route['save'] = 'auth_user/store';

/*
| -------------------------------------------------------------------------
| struktur folder => 'path/to/file';
| -------------------------------------------------------------------------
*/
$route['user'] = 'user/user';
$route['user/(:any)'] = 'user/user/$1';
$route['user/(:any)/(:any)'] = 'user/user/$1/$2';
$route['users-profile/(:any)'] = 'user/user/profile/$1';
$route['users/anggota-profile/(:any)'] = 'user/user/anggota_profile/$1';
$route['registrasi-perusahaan'] = 'user/user/registrasi';
$route['registration-validation'] = 'user/user/validasi_registrasi';
/* --------------------------------------------------------------------- */

/*
| -------------------------------------------------------------------------
| URI for administrator
| -------------------------------------------------------------------------
*/
$route['admin'] = 'admin/admin';
$route['admin/(:any)'] = 'admin/admin/$1';
