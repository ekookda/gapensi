<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
    }

	public function get_data_by_users($table, $where)
	{
		return $this->db->get_where($table,$where);
	}

	public function get_join_table($users_id)
	{
		$this->db->select('*');
		$this->db->from('tb_users');
		$this->db->join('tb_badan_usaha', 'tb_badan_usaha.users_id = tb_users.id_users');
		$this->db->join('tb_propinsi', 'tb_propinsi.id_propinsi = tb_badan_usaha.propinsi_id');
		$this->db->join('tb_kabupaten', 'tb_kabupaten.id_kabupaten = tb_badan_usaha.kabupaten_id');
		$this->db->join('tb_groups_business', 'tb_groups_business.id_groups = tb_badan_usaha.groups_business_id');
		$this->db->where('tb_users.id_users', $users_id);

		return $this->db->get();
	}

}
/* End of file Users_model.php */
