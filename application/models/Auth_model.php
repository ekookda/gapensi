<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_username($table, $where, $limit = 1)
    {
        return $this->db->get_where($table, $where, $limit);
    }

    public function set_save($table, $data)
    {
        $this->db->insert($table, $data);

        return $this->db->insert_id(); // mengembalikan id terakhir yang disimpan
    }

    public function is_active($table, $where)
    {
        $query = $this->db->get_where($table, $where);

        return $query;
    }

    public function verification_email($table, $where)
    {
        $data = array('verification_email' => 1);

        $this->db->where($where);
        $this->db->update($table, $data);

        return TRUE;
    }

    public function set_time_login($table, $where)
    {
        $data = array(
            'ip_address' => $this->input->ip_address(),
            'last_login' => mdate('%Y-%m-%d %H:%i:%s', time())
        );
        $this->db->where($where);

        return $this->db->update($table, $data);
    }

}

/* End of file Auth_model.php */
