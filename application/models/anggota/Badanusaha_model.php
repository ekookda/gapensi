<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Badanusaha_model extends CI_Model 
{

    private $table = 'tb_badan_usaha';

    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_dataAnggota()
    {
        $where = ['id_company'=>$this->session->userdata('id_company')];

        return $this->db->get_where($this->table, $where);
    }

}

/* End of file M_anggota.php */
