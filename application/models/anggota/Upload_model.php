<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_model extends CI_Model
{
    var $table = 'tb_business_license';

    public function __construct()
    {
        parent::__construct();
    }

    public function set_upload($data)
    {
        $query = $this->db->insert($this->table, $data);

        return TRUE;
    }

}
