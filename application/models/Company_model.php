<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Company_model extends CI_Model
{
    private $table = 'tb_badan_usaha';

    public function __construct()
    {
        parent::__construct();
    }

    public function set_store($data = array())
    {
        $this->db->insert($this->table, $data);
        
        return $this->db->insert_id(); // mengembalikan id terakhir yang disimpan
    }

    public function get_company($where)
    {
        $query = $this->db->get_where($this->table, $where);

        return $query->result();
    }

    /* Menampilkan data badan usaha berdasarkan user id
     * @query
     */
    public function getCompanyByUser($where)
    {
        $query = $this->db->get_where('tb_users',$where);

        return $query->result();
    }

}

/* End of file M_login.php */
