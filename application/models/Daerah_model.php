<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Daerah_model extends CI_Model 
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProvinsi()
    {
        $query = $this->db->get('tb_propinsi');

        return $query->result();
    }

    public function getKab($id_provinsi)
    {
        $this->db->order_by('nama_kabupaten', 'ASC');
        $query = $this->db->get_where('tb_kabupaten', ['id_propinsi' => $id_provinsi]);

        return $query;    
    }

}

/* End of file Daerah_model.php */
