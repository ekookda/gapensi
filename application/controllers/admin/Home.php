<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
    }

    public function isLoggedIn()
    {
        if ($this->session->userdata('isLoggedIn') !== TRUE):
            $this->session->set_flashdata('error', 'Anda belum login');
            redirect('login');
        endif;
    }

    public function index()
    {
        echo 'Selamat datang di halaman Admin';
        
        echo "<pre>";
        print_r ($_SESSION);
        echo "</pre>";
        
        echo '<br><a href="'. site_url('login/signout') .'">Logout</a>';
    }

}

/* End of file Home.php */
