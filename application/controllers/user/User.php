<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    private $required = "%s wajib diisi";

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        $this->load->model(array('users/users_model', 'auth_model'));
    }

    public function isLoggedIn()
    {
        $loggedIn = $this->session->userdata('isLoggedIn');
        $level = $this->session->userdata('level');

        if ($loggedIn == FALSE || $level != '2')
        {
            redirect('login');
        }
    }

    public function sendmail($from, $fromName, $to, $subject, $body)
    {
        $this->load->library('email');

        $result =  $this->email
                        ->from($from, $fromName)
                        ->to($to)
                        ->subject($subject)
                        ->message($body)
                        ->send();

        return $result;
    }

    public function verification($key)
    {
        // cek apakah sudah pernah verification_email sebelumnya
        $where = ['md5(id_company)' => $key];
        $isActive = $this->auth_model->is_active('tb_badan_usaha', $where);

        if ($isActive->num_rows() == 1)
        {
            $isActive = $isActive->result();
            if ($isActive[0]->verification_email == 1)
            {
                $this->session->set_flashdata('success', 'Akun sudah pernah diverifikasi. Silahkan Login');
                redirect('masuk');
            }
            else
            {
                $this->auth_model->verification_email('tb_badan_usaha', array('md5(id_company)' => $key));
                $this->session->set_flashdata('success', 'Selamat, Akun anda telah diaktifkan');
                redirect('masuk');
            }
        }
    }

    public function index()
    {
        // Ambil ID Users
        $users_data = $this->users_model->get_data_by_users('tb_users', array('email' => $this->session->userdata('email')))->result();
        $users_id = $users_data[0]->id_users;

        //simpan id users ke session
        $this->session->set_userdata('id_users', $users_id);

        // show data perusahaan by ID users
        $company = $this->users_model->get_join_table($users_id);

        $data = array(
            'title' => 'Data Badan Usaha',
            '_content' => 'user/index',
            'data_usaha' => $company->result()
        );
        // print_r($data);

        $this->load->view('template/main', $data);
    }

    public function profile()
    {
        echo 'Halaman Profile User';
    }

    public function registrasi()
    {
        $data = array(
            'title' => 'Registrasi Perusahaan',
            '_content' => 'user/v_registrasi_usaha'
        );

        $this->load->view('template/main', $data);
    }

    public function validasi_registrasi()
    {
        $this->load->helper('security');

        // redirect jika mem-bypass form
        if (!isset($_POST['btn-submit'])) { redirect('registrasi-perusahaan'); }

        // 1. Kategori Usaha
        $this->form_validation->set_rules('kategori', 'Jenis Usaha', 'required', array('required' => $this->required));

        // 2. Nama Perusahaan
        $this->form_validation->set_rules('company_name', 'Nama Perusahaan', 'required', array('required' => $this->required));

        // 3. Nomor NPWP
        $this->form_validation->set_rules('nomor_npwp', 'Nomor NPWP', 'required', array('required' => $this->required));

        // 4. Propinsi
        $this->form_validation->set_rules('propinsi', 'Propinsi', 'required', array('required' => $this->required));

        // 5. Kabupaten
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required', array('required' => $this->required));

        // 6. Nomor Handphone
        $this->form_validation->set_rules('phone', 'No Handphone', 'required', array('required' => $this->required));

        // 7. Email
        $this->form_validation->set_rules(
            'email', 'Email', 'trim|required|max_length[50]|valid_email|is_unique[tb_badan_usaha.email]',
            array(
                'required'    => $this->required,
                'valid_email' => 'Email tidak valid',
                'is_unique'   => '%s sudah terdaftar'
            )
        );

        // 8. Password
        $this->form_validation->set_rules(
            'password', 'Password', 'trim|required|min_length[8]',
            array(
                'required'   => $this->required,
                'min_length' => '%s minimal 8 karakter'
            )
        );

        // 9. Konfirmasi Password
        $this->form_validation->set_rules(
            'confirm_pwd', 'Konfirmasi Password', 'required|matches[password]',
            array(
                'matches'  => 'Konfirmasi password tidak sama',
                'required' => $this->required
            )
        );

        if ($this->form_validation->run() == FALSE)
        {
            $this->registrasi();
        }
        else
        {
            // get value from form Registrasi
            $dataRegistrasiBadanUsaha = array(
                'ip_address'         => $this->security->xss_clean($this->input->ip_address()),
                'users_id'           => $this->security->xss_clean($this->session->userdata('id_users')),
                'email'              => $this->security->xss_clean($this->input->post('email')),
                'password'           => $this->security->xss_clean($this->input->post('password')),
                'salt'               => $this->security->xss_clean(password_hash($this->input->post('password'), PASSWORD_BCRYPT)),
                'company_name'       => $this->security->xss_clean($this->input->post('company_name')),
                'kabupaten_id'       => $this->security->xss_clean($this->input->post('kabupaten')),
                'propinsi_id'        => $this->security->xss_clean($this->input->post('propinsi')),
                'groups_business_id' => $this->security->xss_clean($this->input->post('kategori')),
                'no_npwp'            => $this->security->xss_clean($this->input->post('nomor_npwp')),
                'phone'              => $this->security->xss_clean($this->input->post('phone')),
                'created_on'         => $this->security->xss_clean(date('Y-m-d H:i:s')),
                'status_process'     => $this->security->xss_clean('pending'),
                'verification_email' => $this->security->xss_clean('0'),
                'activated'          => $this->security->xss_clean('0')
            );

            // Insert to table tb_badan_usaha
            $insert = $this->auth_model->set_save('tb_badan_usaha', $dataRegistrasiBadanUsaha);

            // hash ID to MD5
            $encrypt_id = do_hash($insert, 'md5');

            // Prepare sendmail
            ob_start();

            // Prepare to send mail
            $from     = 'eko.okda@ekoalfarisi.web.id';
            $fromName = 'Eko Okda Alfarisi';
            $to       = $this->input->post('email');
            $subject  = 'Verifikasi Akun';
            $message  = '<p>Terima kasih telah melakukan registrasi. Untuk memverifikasi, silahkan klik <a href="'.site_url('user/verification/' . $encrypt_id).'">Verifikasi</a></a></p>';
            $body     = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http{//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http{//www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
                    <title>' . html_escape($subject) . '</title>
                    <style type="text/css">
                        body {
                            font-family{ Arial, Verdana, Helvetica, sans-serif;
                            font-size{ 16px;
                        }
                    </style>
                </head>
                <body>
                    ' . $message . '
                </body>
                </html>';
                // Also, for getting full html you may use the following internal method{
                //$body = $this->email->full_html($subject, $message);

            $this->sendmail($from, $fromName, $to, $subject, $body);

            $this->session->set_flashdata('success', 'Registrasi berhasil. silahkan cek email untuk verifikasi');

            redirect('registrasi-perusahaan');

            ob_flush();
        }
    }

    public function anggota_profile($id)
    {
        
    }

}

/* End of file User.php */
