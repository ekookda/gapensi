<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['upload']);
    }

    public function do_upload()
    {
        // config upload
        $config = [
            'upload_path' => './assets/uploads',
            'allowed_types' => 'jpg|pdf',
            'encrypt_name' => TRUE,
            'file_name' => date('Ymd').'_'.$this->session->userdata('username'),
        ];
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('btn-upload'))
        {
            $error = ['error' => $this->upload->display_errors()];
            var_dump($error);
            // redirect('member');
        }
        else
        {
            $data = [
                'upload_data' => $this->upload->data(),
            ];

            // $this->load->view($data);
        }
    }

}

/* End of file Upload.php */
