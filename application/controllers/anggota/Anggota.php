<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        $this->load->helper(['date', 'form']);
        $this->load->model('anggota/badanusaha_model');
    }

    public function isLoggedIn()
    {
        $sessLogin = $this->session->userdata('isLoggedIn');
        $sessLevel = $this->session->userdata('level');

        if ($sessLogin == false && $sessLevel != 'anggota')
        {
            $this->session->set_flashdata('notif', 'Anda belum melakukan login!');
            redirect('login');
        }
    }

    /* 
    ** Setelah anggota login, anggota mencetak data yang telah di registrasi.
    */
    public function index()
    {
        // Ambil data anggota dari database
        $result = $this->badanusaha_model->get_dataAnggota();

        if ($result->num_rows() == NULL)
        {
            $unset = ['isLoggedIn', 'username', 'captcha'];
            $this->session->unset_userdata($unset);

            // Jika data tidak ditemukan / tidak sesuai, maka hapus semua session dan direct ke halaman login
            $this->session->set_flashdata('notif', 'Data tidak ditemukan');

            redirect('login');
        }
        else
        {
            // Data ditemukan
            foreach ($result->result() as $row)
            {
                $data['dataAnggota'] = $row;
            }

            $data = [
                'title'    => 'Page Member',
                '_content' => 'anggota/index'
            ];

            $this->load->view('template/main', $data);
        }
    }

    public function upload()
    {
        $data = [
            '_content' => 'anggota/upload_doc_view',
            'title'    => 'Upload File'
        ];

        $this->load->view('template/main', $data);
    }

    public function do_upload()
    {
        $this->load->library('upload');

        $config = [
            'upload_path'   => './assets/uploads/',
            'allowed_types' => 'pdf|jpg|png|jpeg',
            'max_size'      => 4096,
            'remove_spaces' => TRUE,
            'file_name'     => strtolower(date('Ymd') . '_' . $this->session->userdata('company_name'))
        ];
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('txt_upload'))
        {
            $error = ['error' => $this->upload->display_errors()];
            var_dump($error);
        }
        else 
        {
            // Simpan ke database table business_license
            $upload = $this->upload->data();
            $data = [
                'company_id'       => $this->session->userdata('id_company'),
                'path_siup'        => $upload['file_name'],
                'siup_upload_date' => date('Y-m-d H:i:s')
            ];

            $this->load->model('anggota/upload_model');
            $this->upload_model->set_upload($data);
            redirect('member/upload');
        }
    }

}

/* End of file Home.php */
