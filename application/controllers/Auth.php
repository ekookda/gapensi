<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
    private $required = "%s wajib diisi";

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['form', 'captcha', 'date']);
        $this->load->library(['form_validation']);
        $this->load->model(['auth_model'=>'auth_model', 'daerah_model'=>'daerah']);
    }

    public function index()
    {
        redirect('login');
    }

    public function login()
    {
        $captcha = $this->create_captcha();

        // Jika sudah pernah ada session captcha, maka hapus session captcha lama lalu buat session captcha baru
        // if ($this->session->userdata('captcha')) $this->session->unset_userdata('captcha');

        $this->session->set_userdata('captcha', $captcha['word']);

        $data = array(
            'captcha' => $captcha,
            'title' => 'Form Login',
            'msg' => 'MEMBER',
            'label' => 'Email / Nomor NPWP',
            'placeholder' => 'Masukkan Email atau Nomor NPWP Perusahaan',
            'type_input' => 'email'
        );

        $this->load->view('v_login', $data);
    }

    public function create_captcha()
    {
        // Prepare view captcha
        $vals = array(
            'img_path'    => './assets/captcha/',
			'img_url'	  => base_url() . 'assets/captcha',
            // 'img_width'   => 200,
            'img_height'  => 50,
            'expiration'  => 7200,
            'word_length' => 5,
            'font_size'   => 48,
            'img_id'      => 'captchaId',
            'pool'        => '0123456789',
            'colors'      => array(
                'background' => array(255, 255, 255),
                'border'     => array(0, 0, 0),
                'text'       => array(0, 0, 0),
                'grid'       => array(255, 40, 40)
            )
        );

        $captcha = create_captcha($vals);

        return $captcha;
    }

    public function submit()
    {
        if (!isset($_POST['btn-submit']))
        {
            redirect('login');
        }
        else
        {
            // rule validasi form
            $this->form_validation->set_rules('username', 'Username', 'trim|required', array('required' => $this->required));
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]', array('required' => $this->required, 'min_length'=>'%s minimal 8 karakter'));
	        $this->form_validation->set_rules('captcha', 'Kode captcha', 'trim|required', array('required' => $this->required));

            if ($this->form_validation->run() === FALSE)
            {
                $this->login(); // validasi gagal
            }
            else
            {
				// lolos validasi, ambil nilai dari form
                $username = $this->security->xss_clean($this->input->post('username'));
                $password = $this->security->xss_clean($this->input->post('password'));
	            $inputCaptcha = $this->input->post('captcha');
                $sess_captcha = $this->session->userdata('captcha');

                if ($inputCaptcha != $sess_captcha || empty($sess_captcha))
                {
                    // kode captcha tidak cocok antara input dengan session
                    $this->session->set_flashdata('notif', 'Kode Captcha tidak cocok');
                    $this->login();
                }
                else
                {
                    $where = "email ='$username' OR no_npwp = '$username'";
                    $query = $this->auth_model->get_username('tb_badan_usaha', $where); // Ambil data dari tabel login dan cocokkan dengan clausa diatas

                    if ($query->num_rows() != 1)
                    {
                        // Username tidak ditemukan
                        $this->session->set_flashdata('notif', 'Username tidak ditemukan');
                        $this->login();
                    }
                    else
                    {
                        // Username ditemukan, ambil data dari database
                        foreach ($query->result() as $key)
                        {
                            $check_pass = password_verify($password, $key->salt);
                            // Validasi value form dengan database
                            if ($check_pass === TRUE && $key->verification_email == '1')
                            {
                                // Hapus session captcha
                                $this->session->unset_userdata('captcha');

                                // Simpan beberapa data kedalam session
                                $session_data = [
                                    'id_company'   => $key->id_company,
                                    'company_name' => $key->company_name,
                                    'level'        => 'anggota',
                                    'isLoggedIn'   => true
                                ];
                                $this->session->set_userdata($session_data);

                                // update ip dan last login di table
                                $update = $this->auth_model->set_time_login('tb_badan_usaha', array('id_company' => $key->id_company));

                                redirect('member');
                            }
                            elseif ($check_pass === TRUE && $key->verification_email == '0')
                            {
                                // User belum verifikasi email
                                $this->session->set_flashdata('notif', 'Anda belum memverifikasi email');
                                $this->login();
                            }
                            else
                            {
                                // Password tidak cocok
                                $this->session->set_flashdata('notif', 'Password tidak cocok');
                                $this->login();
                            }
                        }
                    }
                }
            }
        }
    }

    public function signout()
    {
        $this->session->sess_destroy();
        redirect('login', 'refresh');
    }

    /**
     * Create or register user
     *
     * @return void
     */
    public function create_member()
    {
        $data = array(
            'title'    => 'Register Anggota'
        );

        $this->load->view('v_register_user', $data);
    }

    public function getKab($id_provinsi)
    {
        $kab = $this->daerah->getKab($id_provinsi);

        if($kab->num_rows() == 0 || $kab->num_rows() == NULL)
        {
            echo "<option value='0'>-- Pilih Kabupaten --</option>";
        }
        else
        {
            echo "<option value='0'>-- Pilih Kabupaten/Kota --</option>";
            foreach($kab->result() as $k)
            {
                echo "<option value='".$k->id_kabupaten."'>".ucwords(strtolower($k->nama_kabupaten))."</option>";
            }
        }
    }

    public function store()
    {
        // redirect jika form tidak di submit
        if (!isset($_POST['btn-submit'])) { redirect('daftar'); }

        // 1. User Fullname
        $this->form_validation->set_rules('fullname', 'Nama', 'required', array('required' => $this->required));

        // 2. Handphone
        $this->form_validation->set_rules('phone', 'No Handphone', 'required', array('required' => $this->required));

        // 3. Email
        $this->form_validation->set_rules(
            'email', 'Email', 'trim|required|max_length[50]|valid_email|is_unique[tb_users.email]',
            array(
                'required'    => $this->required,
                'valid_email' => 'Email tidak valid',
                'is_unique'   => '%s sudah terdaftar'
            )
        );

        // 4. Password
        $this->form_validation->set_rules(
            'password', 'Password', 'trim|required|min_length[8]',
            array(
                'required'   => $this->required,
                'min_length' => '%s minimal 8 karakter'
            )
        );

        // 5. Konfirmasi Password
        $this->form_validation->set_rules(
            'conf_pwd', 'Konfirmasi Password', 'required|matches[password]',
            array(
                'matches'  => 'Konfirmasi password tidak sama',
                'required' => $this->required
            )
        );

        # Execute Form Validation
        if ($this->form_validation->run() === FALSE)
        {
            // Gagal validasi register member
            $this->create_member();
        }
        else
        {
            // ambil nilai dari form user register
            $form_users = array(
                'email'    => $this->security->xss_clean($this->input->post('email')),
                'password' => $this->security->xss_clean($this->input->post('password')),
                'name'     => $this->security->xss_clean($this->input->post('fullname')),
                'phone'    => $this->security->xss_clean($this->input->post('phone')),
                'created_on' => date('Y-m-d H:i:s')
            );

            // Insert ke table user
            $last_users_id = $this->auth_model->set_save('tb_users', $form_users); // return last ID user

            $encrypt_id = md5($last_users_id);

            ob_start();

            // Prepare to send mail
            $from     = 'eko.okda@ekoalfarisi.web.id';
            $fromName = 'Eko Alfarisi';
            $to       = $form_users['email'];
            $subject  = 'Verifikasi Akun';
            $message  = '<p>Terima kasih telah melakukan registrasi. Untuk memverifikasi, silahkan klik <a href="'.site_url('auth/verification/' . $encrypt_id).'">Verifikasi</a></a></p>';
            $body     = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http{//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http{//www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
                    <title>' . html_escape($subject) . '</title>
                    <style type="text/css">
                        body {
                            font-family{ Arial, Verdana, Helvetica, sans-serif;
                            font-size{ 16px;
                        }
                    </style>
                </head>
                <body>
                    ' . $message . '
                </body>
                </html>';
                // Also, for getting full html you may use the following internal method{
                //$body = $this->email->full_html($subject, $message);

            $this->sendmail($from, $fromName, $to, $subject, $body);

            $this->session->set_flashdata('success', 'Registrasi berhasil. silahkan cek email untuk verifikasi');

            redirect('daftar');

            ob_flush();
        }
    }

    public function verification($key)
    {
        // cek apakah sudah pernah verification_email sebelumnya
        $where = ['verification_email' => 1];
        $isActive = $this->auth_model->is_active('tb_users', $where);

        if ($isActive == 0)
        {
            $this->session->set_flashdata('success', 'Akun sudah pernah verifikasi email');

            redirect('masuk');
        }
        else
        {
            $this->auth_model->verification_email($key);

            $this->session->set_flashdata('success', 'Selamat, Akun anda telah diaktifkan');

            redirect('masuk');
        }
    }

    public function sendmail($from, $fromName, $to, $subject, $body)
    {
        $this->load->library('email');

        $result =  $this->email
                        ->from($from, $fromName)
                        ->to($to)
                        ->subject($subject)
                        ->message($body)
                        ->send();

        return $result;
    }

}

/* End of file Auth.php */
