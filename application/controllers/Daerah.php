<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Daerah extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('daerah_model', 'daerah');
    }

    public function index()
    {
        $data['provinsi'] = $this->daerah->getProvinsi();
        print_r($data);
        $this->load->view('welcome_message', $data);
    }

}

/* End of file Daerah.php */
