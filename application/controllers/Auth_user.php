<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_user extends CI_Controller 
{
    /**
     * This Class and method used for register and login users
     * where users can register the company
     * 
     * @return void
     */

    private $required = "%s wajib diisi";

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['form', 'captcha', 'date']);
        $this->load->library(['form_validation']);
        $this->load->model(['auth_model'=>'auth_model', 'daerah_model'=>'daerah']);
    }

    public function index()
    {
        redirect('masuk');
    }

    public function create_captcha()
    {
        // Prepare view captcha
        $vals = array(
            'img_path'    => './assets/captcha/',
			'img_url'	  => base_url() . 'assets/captcha',
            // 'img_width'   => 200,
            'img_height'  => 50,
            'expiration'  => 7200,
            'word_length' => 5,
            'font_size'   => 48,
            'img_id'      => 'captchaId',
            'pool'        => '0123456789',
            'colors'      => array(
                'background' => array(255, 255, 255),
                'border'     => array(0, 0, 0),
                'text'       => array(0, 0, 0),
                'grid'       => array(255, 40, 40)
            )
        );

        $captcha = create_captcha($vals);

        return $captcha;
    }

    public function verification($key)
    {
        // cek apakah sudah pernah verification_email sebelumnya
        $where = ['verification_email' => 1];
        $isActive = $this->auth_model->is_active('tb_login', $where);

        if ($isActive == 1)
        {
            $this->session->set_flashdata('error', 'Akun sudah pernah verifikasi email');

            redirect('masuk');
        }
        else
        {
            $this->auth_model->verification_email('tb_login', array('md5(id_login)' => $key));
            $this->session->set_flashdata('success', 'Selamat, Akun anda telah diaktifkan');

            redirect('masuk');
        }
    }

    public function sendmail($from, $fromName, $to, $subject, $body)
    {
        $this->load->library('email');

        $result =  $this->email
                        ->from($from, $fromName)
                        ->to($to)
                        ->subject($subject)
                        ->message($body)
                        ->send();

        return $result;
    }

    public function signup_user()
    {
        $data['title'] = 'Form Registrasi User';
        $this->load->view('v_register_user', $data);
    }

    public function store()
    {
        // redirect jika form tidak di submit
        if (!isset($_POST['btn-submit'])) { redirect('daftar'); }

        // 1. User Fullname
        $this->form_validation->set_rules('fullname', 'Nama', 'required', array('required' => $this->required));
        
        // 2. Handphone
        $this->form_validation->set_rules('phone', 'No Handphone', 'required', array('required' => $this->required));

        // 3. Email
        $this->form_validation->set_rules(
            'email', 'Email', 'trim|required|max_length[50]|valid_email|is_unique[tb_users.email]',
            array(
                'required'    => $this->required,
                'valid_email' => 'Email tidak valid',
                'is_unique'   => '%s sudah terdaftar'
            )
        );

        // 4. Password
        $this->form_validation->set_rules(
            'password', 'Password', 'trim|required|min_length[8]',
            array(
                'required'   => $this->required,
                'min_length' => '%s minimal 8 karakter'
            )
        );

        // 5. Konfirmasi Password
        $this->form_validation->set_rules(
            'conf_pwd', 'Konfirmasi Password', 'required|matches[password]',
            array(
                'matches'  => 'Konfirmasi password tidak sama',
                'required' => $this->required
            )
        );

        # Execute Form Validation
        if ($this->form_validation->run() === FALSE)
        {
            // Gagal validasi register member
            $this->create_member();
        }
        else
        {
            // Prepare insert table login
            $tb_login = array(
                'ip_address' => $this->input->ip_address(),
                'email' => $this->security->xss_clean($this->input->post('email')),
                'password' => $this->security->xss_clean(password_hash($this->input->post('password'), PASSWORD_BCRYPT)),
                'level' => '2', // 2 = level user
                'last_login' => NULL,
                'verification_email' => 0
            );
            // Insert table return last ID
            $insert_tbLogin = $this->auth_model->set_save('tb_login', $tb_login);

            // ambil nilai dari form user register
            $form_users = array(
                'email' => $this->security->xss_clean($this->input->post('email')),
                'password' => $this->security->xss_clean($this->input->post('password')),
                'name' => $this->security->xss_clean($this->input->post('fullname')),
                'phone' => $this->security->xss_clean($this->input->post('phone')),
                'created_on' => date('Y-m-d H:i:s')
            );

            // Insert ke table user
            $this->auth_model->set_save('tb_users', $form_users); // return last ID user

            // Last ID from tb_login, enkripsi
            $encrypt_id = md5($insert_tbLogin);

            ob_start();

            // Prepare to send mail
            $from     = 'eko.okda@ekoalfarisi.web.id';
            $fromName = 'Eko Alfarisi';
            $to       = $form_users['email'];
            $subject  = 'Verifikasi Akun';
            $message  = '<p>Terima kasih telah melakukan registrasi. Untuk memverifikasi, silahkan klik <a href="'.site_url('auth_user/verification/' . $encrypt_id).'">Verifikasi</a></a></p>';
            $body     = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http{//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http{//www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
                    <title>' . html_escape($subject) . '</title>
                    <style type="text/css">
                        body {
                            font-family{ Arial, Verdana, Helvetica, sans-serif;
                            font-size{ 16px;
                        }
                    </style>
                </head>
                <body>
                    ' . $message . '
                </body>
                </html>';
                // Also, for getting full html you may use the following internal method{
                //$body = $this->email->full_html($subject, $message);

            $this->sendmail($from, $fromName, $to, $subject, $body);

            $this->session->set_flashdata('success', 'Registrasi berhasil. silahkan cek email untuk verifikasi');

            redirect('daftar');

            ob_flush();
        }
    }

    public function signin()
    {
        $captcha = $this->create_captcha();
        
        // Jika sudah pernah ada session captcha, maka hapus session captcha lama lalu buat session captcha baru
        // if ($this->session->userdata('captcha')) $this->session->unset_userdata('captcha');

        $this->session->set_userdata('captcha', $captcha['word']);

        $data = array(
            'captcha' => $captcha,
            'title' => 'Form Login',
            'msg' => 'USERS',
            'label' => 'Email',
            'placeholder' => 'Masukkan email yang valid',
            'type_input' => 'email'
        );

        $this->load->view('v_login_user', $data);
    }

    public function process_login()
    {
        if (!isset($_POST['btn-submit']))
        {
            $this->session->set_flashdata('error', 'Anda belum melakukan login.');
            redirect('masuk');
        } 
        else 
        {
            // rule validasi form
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email', array('required' => $this->required, 'valid_email' => 'Email tidak valid'));
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]', array('required' => $this->required, 'min_length'=>'%s minimal 8 karakter'));
            $this->form_validation->set_rules('captcha', 'Kode captcha', 'trim|required|numeric', array('required' => $this->required, 'numeric' => 'Kode captcha tidak sesuai'));

            if ($this->form_validation->run() === FALSE)
            {
                $this->signin(); // validasi gagal
            } 
            else 
            {
				// lolos validasi, ambil nilai dari form
                $email        = $this->security->xss_clean($this->input->post('email'));
                $password     = $this->security->xss_clean($this->input->post('password'));
                $inputCaptcha = $this->input->post('captcha', TRUE);
                $sess_captcha = $this->session->userdata('captcha');

                if ($inputCaptcha != $sess_captcha || empty($sess_captcha))
                {
                    // kode captcha tidak cocok antara input dengan session
                    $this->session->set_flashdata('error', 'Kode Captcha tidak cocok');
                    $this->signin();
                } 
                else 
                {
                    $where = ['email' => $email];
                    $query = $this->auth_model->get_username('tb_login', $where); // Ambil data dari tabel login dan cocokkan dengan clausa diatas

                    if ($query->num_rows() != 1)
                    {
                        // Username tidak ditemukan
                        $this->session->set_flashdata('error', 'Email belum terdaftar');
                        $this->signin();
                    } 
                    else 
                    {
                        // Username ditemukan, ambil data dari database
                        foreach ($query->result() as $key)
                        {
                            $check_pass = password_verify($password, $key->password);
                            // Validasi value form dengan database
                            if ($check_pass === TRUE)
                            {
                                // Hapus session captcha
                                $this->session->unset_userdata('captcha');

                                if ($key->verification_email == 0)
                                {
                                    $this->session->set_flashdata('error', 'Anda belum verifikasi email');
                                    redirect('masuk');
                                }

                                // update ip dan last login di table
                                $update = $this->auth_model->set_time_login('tb_login', array('email' => $email));

                                if ($key->level == '1')
                                {
                                    // level admin
                                    $query = $this->auth_model->get_username('tb_admin', $where)->result();
                                    $this->session->set_flashdata('success', 'Anda login sebagai admin');
                                    $path = 'admin';
                                }
                                elseif ($key->level == '2')
                                {
                                    // level user
                                    $query = $this->auth_model->get_username('tb_users', $where)->result();
                                    $this->session->set_flashdata('success', 'Anda login sebagai User');
                                    $path = 'user';
                                }
                                else
                                {
                                    // level tidak ada / kosong, redirect ke menu login
                                    $path = 'masuk';
                                }

                                // Simpan beberapa data kedalam session
                                $session_data = array(
                                    'email' => $key->email,
                                    'level' => $key->level,
                                    'isLoggedIn' => TRUE,
                                    'name' => $query[0]->name
                                );

                                $this->session->set_userdata($session_data);

                                redirect($path);
                            } 
                            else 
                            {
                                // Password tidak cocok
                                $this->session->set_flashdata('error', 'Password tidak cocok');
                                $this->signin();
                            }
                        }
                    }
                }
            }
        }
    }

    public function signout()
    {
        $this->session->sess_destroy();

        redirect('masuk');
    }

}

/* End of file Auth_user.php */
