$(document).ready(function () {
	$('#table-perusahaan').DataTable();

	// Mask Input Registrasi
	$("#npwp").mask("99.999.999.9-999.999");

	$("#phone").mask("0000000000000", {
		placeholder: 'contoh: 081234567890'
	});

});