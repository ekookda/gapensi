-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 31 Des 2017 pada 17.47
-- Versi Server: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gapensi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_anggota`
--

CREATE TABLE `tb_anggota` (
  `id_anggota` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `groups_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_anggota`
--

INSERT INTO `tb_anggota` (`id_anggota`, `ip_address`, `username`, `password`, `email`, `groups_id`, `created_on`, `last_login`, `active`) VALUES
(1, '::1', 'root', '$2y$10$x8.wDklbDofmKoU0cm5YN.tn03FmsercWgW7kp570cVzOyw5QvyNW', 'eko.okda@outlook.com', 3, '2017-12-31 04:59:48', '2017-12-31 06:01:05', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_anggota_old`
--

CREATE TABLE `tb_anggota_old` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `alamat_perusahaan` text NOT NULL,
  `telepon` char(15) DEFAULT NULL,
  `fax` char(15) DEFAULT NULL,
  `email` char(100) NOT NULL,
  `jenis_usaha` enum('pt','cv','firma','koperasi') NOT NULL,
  `level_anggota` enum('pusat','daerah') NOT NULL COMMENT 'level anggota gapensi',
  `status` enum('0','1') NOT NULL COMMENT '0=belum aktif, 1=aktif',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_anggota_old`
--

INSERT INTO `tb_anggota_old` (`id`, `username`, `nama_perusahaan`, `alamat_perusahaan`, `telepon`, `fax`, `email`, `jenis_usaha`, `level_anggota`, `status`, `created_at`, `updated_at`) VALUES
(1, 'root', 'Rury Fastro', 'Jl. Utama (pertigaan Al-Huda) No.7 Cengkareng, Jakarta Barat', '0213456789012', '', 'eko.okda@gmail.com', 'cv', 'pusat', '0', '2017-12-28 09:41:13', '2017-12-28 17:08:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_bisnis_kategori`
--

CREATE TABLE `tb_bisnis_kategori` (
  `id_kategori` int(11) NOT NULL,
  `kode_kategori` varchar(30) NOT NULL,
  `deskripsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_bisnis_kategori`
--

INSERT INTO `tb_bisnis_kategori` (`id_kategori`, `kode_kategori`, `deskripsi`) VALUES
(1, 'pt', 'Perseroan Terbatas'),
(2, 'firma', 'Firma'),
(3, 'cv', 'Persekutuan Komanditer'),
(4, 'koperasi', 'Koperasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_company`
--

CREATE TABLE `tb_company` (
  `id_company` int(10) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `director` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `kategori_id` int(11) NOT NULL COMMENT 'relasi dari tabel bisnis kategori',
  `npwp` char(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_company`
--

INSERT INTO `tb_company` (`id_company`, `username`, `company_name`, `address`, `director`, `phone`, `fax`, `kategori_id`, `npwp`) VALUES
(1, 'root', 'Rury Fastro', 'Jl. Utama (pertigaan Al-Huda) No.7 Cengkareng, Jakarta Barat', '', '02512345678901', '', 2, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_groups`
--

CREATE TABLE `tb_groups` (
  `id` tinyint(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tabel level user/akses';

--
-- Dumping data untuk tabel `tb_groups`
--

INSERT INTO `tb_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'administrator'),
(2, 'pusat', 'gapensi pusat'),
(3, 'daerah', 'gapensi daerah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu`
--

CREATE TABLE `tb_menu` (
  `id` int(11) NOT NULL,
  `menu_title` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `is_main_menu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_propinsi`
--

CREATE TABLE `tb_propinsi` (
  `id_propinsi` int(11) NOT NULL,
  `nama_propinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_propinsi`
--

INSERT INTO `tb_propinsi` (`id_propinsi`, `nama_propinsi`) VALUES
(11, 'Nanggroe Aceh Darussalaam'),
(12, 'Sumatra Utara'),
(13, 'Sumatra Barat'),
(14, 'Riau'),
(15, 'Jambi'),
(16, 'Sumatra Selatan'),
(17, 'Bengkulu'),
(18, 'Lampung'),
(19, 'Kep. Bangka Belitung'),
(21, 'Kep. Riau'),
(31, 'DKI Jakarta'),
(32, 'Jawa Barat'),
(33, 'Jawa Tengah'),
(34, 'DI Yogyakarta'),
(35, 'Jawa Timur'),
(36, 'Banten'),
(51, 'Bali'),
(52, 'Nusa Tenggara Barat'),
(53, 'Nusa Tenggara Timur'),
(61, 'Kalimantan Barat'),
(62, 'Kalimantan Tengah'),
(63, 'Kalimantan Selatan'),
(64, 'Kalimantan Timur'),
(71, 'Sulawesi Utara'),
(72, 'Sulawesi Tengah'),
(73, 'Sulawesi Selatan'),
(74, 'Sulawesi Tenggara'),
(75, 'Gorontalo'),
(81, 'Maluku'),
(82, 'Maluku Utara'),
(91, 'Papua'),
(92, 'Irian Jaya Barat');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_anggota`
--
ALTER TABLE `tb_anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `tb_anggota_old`
--
ALTER TABLE `tb_anggota_old`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `tb_bisnis_kategori`
--
ALTER TABLE `tb_bisnis_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tb_company`
--
ALTER TABLE `tb_company`
  ADD PRIMARY KEY (`id_company`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `tb_groups`
--
ALTER TABLE `tb_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_propinsi`
--
ALTER TABLE `tb_propinsi`
  ADD PRIMARY KEY (`id_propinsi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_anggota`
--
ALTER TABLE `tb_anggota`
  MODIFY `id_anggota` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_anggota_old`
--
ALTER TABLE `tb_anggota_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_bisnis_kategori`
--
ALTER TABLE `tb_bisnis_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_company`
--
ALTER TABLE `tb_company`
  MODIFY `id_company` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_groups`
--
ALTER TABLE `tb_groups`
  MODIFY `id` tinyint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_menu`
--
ALTER TABLE `tb_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_propinsi`
--
ALTER TABLE `tb_propinsi`
  MODIFY `id_propinsi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
